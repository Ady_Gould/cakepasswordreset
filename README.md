# CakePHP Reset Password Demo

[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A quick demo application to show one method of password resetting using an
email to the end user with a link for them to click to reset their password.

Application built upon [CakePHP](http://cakephp.org) 3.x.

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar install` to add the CakePHP application framework
 and required plugins.

If Composer is installed globally, run
```bash
composer install
```

## Configuration

Create a database and username such as XXX_forgot with a password that is
easy to remember, such as PassWord, for this demonstration.

Edit `config/app.php` and setup the 'Datasources' with these details.

Edit the `config/app.php` and add an smtp email transport with a port that
is set up for a testing SMTP server (such as Papercut or MockSmtp or

```php
'EmailTransport' => [
        'default' => [
            'className' => 'Mail',
            // The following keys are used in SMTP transports
            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'username' => 'user',
            'password' => 'secret',
            'client' => null,
            'tls' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
        'smtp' => [
            'className' => 'Smtp',
            // The following keys are used in SMTP transports
            'host' => '127.0.0.1',
            'port' => 1025,
            'timeout' => 30,
            //'username' => 'user',
            //'password' => 'secret',
            'client' => null,
            'tls' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
    ],
```

Also change the Email section:

```php
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'no-reply@localhost',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        'smtp' => [
            'transport' => 'smtp',
            'from' => 'no-reply@example.local',
            'charset' => 'utf-8',
            'headerCharset' => 'utf-8',
        ],
    ],
```

Upload the application to the testing server.

## PaperCut (Windows) Configuration

The SMTP testing systems will need to be told:

- What IP Address to listen to
- What Port to listen on

Set the IP Address to 127.0.0.1 and the port to 1025. These changes should
match up with the settings in the EmailTransport section above.


## Testing ##
Open the web address of this test server. For example:
`http://localhost/~name/CPR` in your browser.
