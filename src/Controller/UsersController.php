<?php
namespace App\Controller;

use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Utility\Text;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record
     *                                                            not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add,
     *                                     renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|void Redirects on successful edit,
     *                                     renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not
     *                                                   found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record
     *                                                            not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Forgot Password
     *
     */
    public function forgotpassword()
    {
        $this->Users->recursive = -1;

        if ($this->request->is('post')) {

            if (empty($this->request['data']['email'])) {
                $this->Flash->error('Please provide the eMail address that
                 you used to register with us');
            } else {

                // find the user with the email that was submitted
                $email = $this->request['data']['email'];

                // note we could remove the ->limit(1) to return all rows
                // with the email address, thus double checking if the
                // email is unique. The table structure should ensure the
                // user email and user names are unique no matter what.
                $foundUsers = $this->Users->find()
                                          ->where(['email' => $email])
                                          ->limit(1);

                if ($foundUsers->count() == 1) {

                    foreach ($foundUsers as $oneUser) {

                        if ($oneUser->status == "1") {

                            // create a token for the user to use to reset
                            // password
                            $key = Security::hash(Text::uuid(), 'sha512',
                                true);
                            $hash = sha1($oneUser->email . mt_rand(0, 100));

                            $url = Router::url([
                                    'controller' => 'users',
                                    'action'     => 'reset'
                                ], true) . '/' . $key . '#' . $hash;

                            $ms = "<p>You are receiving this email as you have requested a change of password.</p><p>If you have not requested this change please ignore this email. Click the link below to reset your password... </p><p style='width:100%;'><a href=" . $url . " style='text-decoration:none'> <b>Click me to reset your password.</b></a></p>";

                            $user        = $this->Users->get($oneUser->id);
                            $user->token = $key;

                            if ($this->Users->save($user)) {

                                // send an email using SMTP email transport
                                // as configured in the app.php
                                // configuration file
                                $email = new Email();
                                $this->set('smtp_errors', "none");
                                $email->from(
                                    'no-reply@example.com',
                                    'Forgotton Password')
                                      ->emailFormat('both')
                                      ->to($oneUser->email)
                                      ->template('default', 'default')
                                      ->transport('smtp')
                                      ->send($ms);

                                $this->Flash->success(
                                    __('Check Your Email To Reset your password',
                                        true)
                                );
                                $this->redirect(array(
                                    'controller' => 'Pages',
                                    'action'     => 'display'
                                ));

                            } else {
                                $this->Flash->error("Error Generating Reset link");
                            } // end if updated user saved

                        } else {
                            $this->Flash->error('This Account is Blocked. 
                            Please Contact the Customer Services Centre...');
                        } // end if user enabled

                    } // end foreach

                } else {
                    $this->Flash->error('Sorry, we do not know that person.');
                    $this->redirect(array(
                        'controller' => 'Pages',
                        'action'     => 'display'
                    ));
                } // end if one user found

            } // end if email was empty

        } // end if form was posted

    } // end forgot password method


    /**
     * Reset Password
     *
     * @param null $token
     */
    public function reset($token = null)
    {
        $this->Users->recursive = -1;

        // if the token is not empty we can start the reset process
        if ( ! empty($token)) {

            $foundUsers = $this->Users->find()
                                      ->where(['token' => $token])
                                      ->limit(1);

            // if we have found **one user** with the token then proceed
            if ($foundUsers->count() == 1) {

                // check if the form "post" request contains data
                if ( ! empty($this->request['data'])) {

                    // check that the new password matches with the confirm
                    // password, if not flash an error up.
                    if ($this->request['data']['password'] !==
                        $this->request['data']['password_confirm']
                    ) {
                        $this->Flash->error("Password does not match.");

                        return;
                    }

                    // grab the new password from the form
                    $newPass = $this->request['data']['password'];
                    // grab the user email from the oe we found
                    $userMail = $foundUsers->first()->email;

                    //create update to token (invalidates previous token)
                    $newHash = sha1($userMail . mt_rand(0, 100));

                    // put new data into array for saving
                    $data = [
                        'passwd' => $newPass,
                        'token'  => $newHash
                    ];

                    // patch the user we found with the data we have
                    $foundUsers = $this->Users->patchEntity(
                        $foundUsers->first(), $data
                    );

                    // attempt to save the user's new details
                    if ($this->Users->save($foundUsers)) {

                        $this->Flash->success('Password Has been Updated.');
                        $this->redirect([
                            'controller' => 'Users',
                            'action'     => 'login'
                        ]);
                    } // end save updated password

                } // end non empty post request

            } else {
                $this->Flash->error('Token Corrupted. The reset link work only for once.');
                $this->redirect([
                    'controller' => 'Users',
                    'action'     => 'login'
                ]);
            } // end if one user

        } else {
            $this->Flash->error('Pls try again...');

            $this->redirect(array(
                'controller' => 'Users',
                'action'     => 'login'
            ));
        } // end if non empty token

    } // end reset password

    /**
     * Login
     */
    public function login()
    {
        if ($this->request->is('post')) {

            if ( ! empty($this->request['data'])) {
                $passwd = $this->request['data']['password'];
                $email  = $this->request['data']['email'];

                $user = $this->Users->find('all')
                                    ->where(['email' => $email])
                                    ->first();

                if ($passwd == $user->passwd) {
                    $this->Flash->success(__('Logged in'));
                    $this->redirect(array(
                        'controller' => 'Users',
                        'action'     => 'index'
                    ));
                } else {
                    $this->Flash->error(__('Username and/or password was incorrect.'));
                } // end if password match
            } // end form was posted
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        } // end request was a post

    } // end login

} // end class Users
