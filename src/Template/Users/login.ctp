<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <h2>Important</h2>
    <p>Remember that this login is just a test to show a login
       form with a "forgotten password" link. </p>
    <p>There is no actual logging-in to the application.</p>
</nav>
<div class="form large-9 medium-8 columns">

    <h1>Login</h1>
    <?php echo $this->Form->create('Login'); ?>

    <?= $this->Form->input('email'); ?>
    <?= $this->Form->input('password', ['type' => 'password']); ?>

    <?= $this->Form->button('Submit', ['type'=>'submit']); ?>

    <p>
        <?= $this->Html->link("I've forgotten my password.",
            ['controller' => 'Users', 'action' => 'forgotpassword']); ?>
    </p>
    <?php echo $this->Form->end(); ?>

</div>
