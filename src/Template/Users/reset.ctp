<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <h2>Important</h2>
    <p>This reset page uses a link that is sent to the nominated email
       address if it exists in the application's database.</p>
</nav>
<div class="form large-9 medium-8 columns">

    <h1>Reset Password</h1>
    <?= $this->Form->create('User'); ?>
    <?= $this->Form->input('password', [
        'placeholder' => "New Password...",
        'type'        => "password"
    ]); ?>

    <?= $this->Form->input('password_confirm', [
        'placeholder' => "Confirm Password...",
        'type'        => "password"
    ]); ?>

    <?= $this->Form->button('Submit', ['type' => 'submit']); ?>


    <?= $this->Form->end(); ?>


</div>
