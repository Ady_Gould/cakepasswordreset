<nav class="large-3 medium-4 columns" id="actions-sidebar">

</nav>
<div class="form large-9 medium-8 columns">
    <h1>Forgot my password</h1>
    <p>Type in the email address you have registered with us, and submit
       the form.</p>
    <p>An email will be sent with a link to a one time only token
       that you use to reset your password. </p>
    <?php echo $this->Form->create('User'); ?>
    <?= $this->Form->input('email'); ?>
    <?= $this->Form->button('Submit', ['type' => 'submit']); ?>
    <?php echo $this->Form->end(); ?>

</div>
