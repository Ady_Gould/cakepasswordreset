<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc.
 *                (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT
 *                License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

?>
<div id="content">
    <div class="row">
        <div class="columns large-12 ctp-warning checks">
            <h1 class="cake-error">Error</h1>
            <p class="large">
                If you get this page, then something went wrong.
                Try visiting the
                <?=
                $this->Html->link('User Login', [
                    'controller' => 'users',
                    'action'     => 'login'
                ]); ?>
        </div>
    </div>
