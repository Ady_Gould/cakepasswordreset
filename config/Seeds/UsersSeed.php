<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'email'  => 'admin@example.local',
                'passwd' => 'password',
                'status' => 1

            ],
            [
                'email'  => 'rusty@example.local',
                'passwd' => 'password',
                'status' => 1
            ],
            [
                'email'  => 'brian@example.local',
                'passwd' => 'password',
                'status' => 0
            ],
            [
                'email'  => 'ermintrue@example.local',
                'passwd' => 'password',
                'status' => 1
            ],
            [
                'email'  => 'dillon@example.local',
                'passwd' => 'password',
                'status' => 0
            ],
            [
                'email'  => 'florence@example.local',
                'passwd' => 'password',
                'status' => 1
            ],
            [
                'email'  => 'zebedee@example.local',
                'passwd' => 'password',
                'status' => 0
            ],
            [
                'email'  => 'dougal@example.local',
                'passwd' => 'password',
                'status' => 1
            ],
            [
                'email'  => 'jack@example.local',
                'passwd' => 'password',
                'status' => 1
            ],
            [
                'email'  => 'jill@example.local',
                'passwd' => 'password',
                'status' => 0
            ]
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
